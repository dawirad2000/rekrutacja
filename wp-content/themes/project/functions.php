<?php


if ( version_compare( $GLOBALS['wp_version'], '5.3', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( !function_exists( 'twenty_twenty_one_setup' ) ) {

	function twenty_twenty_one_setup() {

		load_theme_textdomain( 'twentytwentyone', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support(
			'post-formats',
			array(
				'link',
				'aside',
				'gallery',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);

		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		register_nav_menus(
			array(
				'primary'       => esc_html__( 'Primary menu', 'twentytwentyone' ),
				'homepage'      => esc_html__( 'Strona Główna menu', 'twentytwentyone' ),
				'footer'        => esc_html__( 'Secondary menu', 'twentytwentyone' ),
				'footer_second' => esc_html__( 'Footer Right menu', 'twentytwentyone' ),
			)
		);

	}
}
add_action( 'after_setup_theme', 'twenty_twenty_one_setup' );

function twenty_twenty_one_widgets_init() {

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer', 'twentytwentyone' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'twentytwentyone' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'twenty_twenty_one_widgets_init' );


function project_scripts() {


	wp_enqueue_style( 'twenty-twenty-one-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'twenty-twenty-one-homepage', get_template_directory_uri() . '/assets/sass/main.css', array(), wp_get_theme()->get( 'Version' ) );


		wp_register_script(
			'customize',
			get_template_directory_uri() . '/assets/js/customize.js',
			array(),
			wp_get_theme()->get( 'Version' ),
			false
		);
		wp_enqueue_script( 'customize-js', get_template_directory_uri() . '/assets/js/customize.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ) );

}

add_action( 'wp_enqueue_scripts', 'project_scripts' );

//Menu
function register_custom_menu() {
    register_nav_menu('menu', 'Menu');
}
add_action('after_setup_theme', 'register_custom_menu');

//Menu Icons
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
    foreach( $items as &$item ) {
        
        $icon = get_field('icon', $item);
        
        if( $icon ) {
            
            $item->title .= wp_get_attachment_image($icon['id'], 'full');
            
        }
        
    }

    return $items;
    
}
