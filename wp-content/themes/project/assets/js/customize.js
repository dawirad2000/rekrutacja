document.addEventListener("DOMContentLoaded", () => {

// Get all submenu elements
const submenus = document.querySelectorAll('.sub-menu');

// Add event listener to each menu item
submenus.forEach(submenu => {
  const menuItem = submenu.previousElementSibling;
  menuItem.addEventListener('click', function(event) {
    // Toggle the display of the submenu
    event.preventDefault;
    submenu.classList.toggle('open');
  });
});

// Get all submenu elements
const hamburger = document.getElementById('burger');
hamburger.addEventListener('click', function() {
    document.querySelector('.menu-wrap').classList.toggle('open')
    console.log(document.querySelector('.menu-wrap'))
})

});