<?php
/**
 * The header.
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header>
	<div class="mini-header">
		<div class="page-container head">
			<div class="text-left">
				<p class="design"><?php echo esc_html( get_field('text_left') ); ?></p>
			</div>
			<div class="text-right">
				<p class="poland"><?php echo esc_html( get_field('text_right_1') ); ?></p>
				<p class="shipping"><?php echo esc_html( get_field('text_right_2') ); ?></p>
				<p class="return"><?php echo esc_html( get_field('text_right_3') ); ?></p>
			</div>
		</div>
	</div>
	<div class="page-container header">
		<div class="logo">
			<a href="#">
		<?php 
			$image = get_field('logo');
			if( !empty( $image ) ): ?>
    			<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
		<?php endif; ?>
			</a>
		</div>
		<div class="left-box">
			<div class="menu-wrap">
				<?php
				wp_nav_menu(array(
    				'theme_location' => 'menu',
    				'menu_class' => 'menu'
				));
				?>
				<div class="add-menu">
					<div class="search-wrap">
						<div class="search">
    						<input type="text" class="search__input" placeholder="Wyszukaj po nazwie...">
    						<button class="search__button">
        					<svg class="search__icon" aria-hidden="true" viewBox="0 0 24 24">
            					<g>
                					<path d="M21.53 20.47l-3.66-3.66C19.195 15.24 20 13.214 20 11c0-4.97-4.03-9-9-9s-9 4.03-9 9 4.03 9 9 9c2.215 0 4.24-.804 5.808-2.13l3.66 3.66c.147.146.34.22.53.22s.385-.073.53-.22c.295-.293.295-.767.002-1.06zM3.5 11c0-4.135 3.365-7.5 7.5-7.5s7.5 3.365 7.5 7.5-3.365 7.5-7.5 7.5-7.5-3.365-7.5-7.5z"></path>
            					</g>
        					</svg>
    						</button>
						</div>
					</div>
					<div class="mail">
						<?php 
						$image = get_field('phone');
						if( !empty( $image ) ): ?>
    					<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						<?php endif; ?>
						<?php 
						$link = get_field('phone_link');
						if( $link ): 
    						$link_url = $link['url'];
    						$link_title = $link['title'];
    						$link_target = $link['target'] ? $link['target'] : '_self';
    						?>
    						<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					</div>
					<div class="phone">
						<?php 
						$image = get_field('mail');
						if( !empty( $image ) ): ?>
    					<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						<?php endif; ?>
						<?php 
						$link = get_field('mail_link');
						if( $link ): 
    						$link_url = $link['url'];
    						$link_title = $link['title'];
    						$link_target = $link['target'] ? $link['target'] : '_self';
    						?>
    						<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<div class="buttons">
			<a href="#"><img src="/wp-content/themes/project/assets/icons/search.svg" alt="Search"></a>
			<a href="#"><img src="/wp-content/themes/project/assets/icons/user.svg" alt="Account"></a>
			<a href="#"><img src="/wp-content/themes/project/assets/icons/shopping-cart.svg" alt="Cart"></a>
		</div>
		</div>
		<div class="mobile">
			<a href="#"><img src="/wp-content/themes/project/assets/icons/shopping-cart.svg" alt="Cart"></a>
			<label class="burger" for="burger">
  				<input type="checkbox" id="burger">
  				<span></span>
  				<span></span>
  				<span></span>
			</label>
		</div>
	</div>
</header>

