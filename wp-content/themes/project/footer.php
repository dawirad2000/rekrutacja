<?php
/**
 * The template for displaying the footer
 */

?>

<footer>
<div class="mobile-footer">
	<a href="#"><img src="/wp-content/themes/project/assets/icons/search.svg" alt="Search"></a>
	<a href="#"><img src="/wp-content/themes/project/assets/icons/user.svg" alt="Account"></a>
	<a href="#"><img src="/wp-content/themes/project/assets/icons/shopping-cart.svg" alt="Cart"></a>
</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
